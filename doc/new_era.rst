new\_era package
================


new\_era.peristaltic\_pump module
---------------------------------

.. automodule:: new_era.peristaltic_pump
   :members:
   :undoc-members:
   :show-inheritance:

new\_era.utils module
---------------------

.. automodule:: new_era.utils
   :members:
   :undoc-members:
   :show-inheritance:


Module contents
---------------

.. automodule:: new_era
   :members:
   :undoc-members:
   :show-inheritance:
