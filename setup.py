import os
import setuptools
from setuptools import setup, find_packages

NAME = 'new_era'

with open("README.md", "r") as fh:
    long_description = fh.read()


# Load the package's __version__.py module as a dictionary.
here = os.path.abspath(os.path.dirname(__file__))
about = {}
with open(os.path.join(here, NAME, '__version__.py')) as f:
    exec(f.read(), about)



REQUIRED = [
    'pyserial',
]

setup(
    name=NAME,
    version=about['__version__'],
    packages=setuptools.find_packages(),
    author='Veronica Lai // Hein Group',
    description='An unofficial library to control New Era pumps. We are not affliated with New Era.',
    long_description=long_description,
    long_description_content_type="text/markdown",
    url='https://gitlab.com/heingroup/new_era',
    author_email='',
    install_requires=REQUIRED,
    python_requires='>=3.6',
)


