"""
Example of a pump network (daisy chaining pumps).
"""

import time
from new_era.peristaltic_pump_network import PeristalticPumpNetwork


def main():
    pump_port = 'COM3'  # check on your own system
    pump_network = PeristalticPumpNetwork(port=pump_port, baudrate=9600)
    ne_pump_1 = pump_network.add_pump(address=0, baudrate=9600)  # first pump directly connected to the computer
    ne_pump_2 = pump_network.add_pump(address=1, baudrate=9600)  # second pump connected to the first pump

    ne_pump_1.start()
    time.sleep(5)
    ne_pump_1.stop()

    ne_pump_2.start()
    time.sleep(5)
    ne_pump_2.stop()


if __name__ == "__main__":
    main()
