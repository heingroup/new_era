# new_era

An unofficial library to control New Era pumps. We are not affliated with New Era.

Supported and tested products:
- [NE-9000 peristaltic pump](http://www.syringepump.com/download/NE-9000%20Peristaltic%20Pump%20User%20Manual.pdf)

This package may also work to control New Era syringe pumps but this has not been tested. 

### Example usage

For control of a individual pumps use the `peristaltic_pump` module. Check in the examples folder for an [example](examples/example_peristaltic_pump.py).

For control of a pump network where pumps are daisy-chained to each other, use the `peristaltic_pump_network` module. Check in the examples folder for an [example](examples/example_pump_network.py).
